# ToyBox_Collisional_Particles

This is a toy Python script to simulate and animate 64 particles moving and colliding within a 2D box.  The figure below shows a snapshot of an example run. In this toy script, collisions are resolved in a brute-force way (so the FPS could be low on a low-spec machine).

<center><img src="./example_run.png" width="450"></center>