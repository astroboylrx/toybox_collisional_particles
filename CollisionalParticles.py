#!/usr/bin/env python3
""" Make an animation of 2D particles.

    Author: Rixin Li
"""

# import modules
import numpy as np
import math
import time
import copy
import sys
#import ipdb
import matplotlib as mpl

# Choose what you have or leave it as it is
mpl.use("Qt5Agg")
#mpl.use("Qt4Agg") # Anaconda may support this

import matplotlib.pyplot as plt
global current_backend
current_backend = plt.get_backend()
if current_backend == "MacOSX":
    # a dirty trick from
    # https://github.com/matplotlib/matplotlib/issues/3505
    # to enable this script to be executed from terminal in Mac
    sys.ps1 = 'SOMETHING'

class vector_2D:
    """Define a 2D vector class for easy vector calculation."""

    def comp_r_angle(self):
        """Compute |r| and angle once (x,y) has changed."""
        self.r = np.sqrt(self.x1 ** 2 + self.x2 ** 2)
        # make sure angle is between (-pi, pi] in radians
        self.angle = math.atan2(self.x2, self.x1)

    def __init__(self, x1, x2):
        """Constructor function"""
        self.x1 = x1
        self.x2 = x2
        self.comp_r_angle()

    def __add__(self, other_value):
        """Overload the return value of self + other_value"""
        temp = copy.deepcopy(self)
        if isinstance(other_value, vector_2D):
            temp.x1 += other_value.x1
            temp.x2 += other_value.x2
        else:
            print("Warning: only vectors can be added onto vectors.")
            print("Now treating", other_value, \
                  "as (" + str(other_value) + ", " + str(other_value) + ").")
            temp.x1 += other_value
            temp.x2 += other_value
        temp.comp_r_angle()
        return temp

    def __radd__(self, other_value):
        """Overload the return value of other_value + self"""
        return self.__add__(other_value)

    def __iadd__(self, other_value):
        """Overload self += other_value."""
        return self.__add__(other_value)

    def __mul__(self, other_value):
        """Overload the return value of dot product: self * other_value"""
        if isinstance(other_value, vector_2D):
            return self.x1 * other_value.x1 + self.x2 * other_value.x2
        else:
            temp = copy.deepcopy(self)
            temp.x1 *= other_value
            temp.x2 *= other_value
            temp.comp_r_angle()
            return temp

    def __rmul__(self, other_value):
        """Overload the return value of dot product: other_value * self"""
        return self.__mul__(other_value)

    def __imul__(self, other_value):
        """Overload self *= other_value"""
        return self.__mul__(other_value)

    def __sub__(self, other_value):
        """Overload the return value of self - other_value"""
        return self.__add__(other_value * (-1))

    def __rsub__(self, other_value):
        """Overload the return value of other_value - self"""
        temp = copy.deepcopy(self)
        temp *= (-1)
        return self.__add__(other_value)

    def __isub__(self, other_value):
        """Overload self -= other_value"""
        return self.__sub__(other_value)

    def __truediv__(self, other_value):
        """Overload division: self / other_value"""
        if isinstance(other_value, vector_2D):
            if other_value.angle != self.angle:
                raise TypeError("A vector cannot be divided by another" \
                                + " vector with a different orientation.")
            else:
                return self.x1 / other_value.x1
        else:
            return self.__mul__(1 / other_value)

    def __rtruediv__(self, other_value):
        """Overload division: other_value / self"""
        if isinstance(other_value, vector_2D):
            if other_value.angle != self.angle:
                raise TypeError("A vector cannot be divided by another" \
                                + " vector with a different orientation.")
            else:
                return other_value.x1 / self.x1
        else:
            raise SyntaxError("A vector cannot be a divisor of a scalar.")

    def __itruediv__(self, other_value):
        """Overload self /= other_value"""
        return self.__div__(other_value)

    def __neg__(self):
        """Overload unary operator -self"""
        temp = copy.deepcopy(self)
        temp *= (-1)
        return temp

    def __pos__(self):
        """Overload unary operator +self"""
        temp = copy.deepcopy(self)
        return temp

    def __abs__(self):
        """Overload unary operator abs(self)"""
        return self.r

    def __pow__(self, other_value):
        """Overload self**other_value"""
        return self.r**other_value

    def showme(self):
        """Print vector info"""
        print("(x,y)=(" + str(self.x1) + ", " + str(self.x2) + "), \
              |r|=" + str(self.r) + ", angle=" + str(self.angle) + "[rad].")

class ParticleSet:
    """Encapsulate the entire particle set and compute their motions."""


    def __init__(self, num_par=100, xlim=[-1, 1], ylim=[-1, 1], \
                 BCs="Reflecting", CFL=0.1, radii=None):
        """Constructor for parSet"""

        # copy keywords as members
        self.num_par = num_par
        self.xlim = np.asarray(xlim)
        self.ylim = np.asarray(ylim)
        self.BCs = BCs
        self.CFL = CFL

        # more members to simplify further calculations
        ## for reflecting BCs
        self.twice_xlim = 2 * self.xlim
        self.twice_ylim = 2 * self.ylim
        ## for particle position initilization and periodic BCs
        self.xlen = xlim[1] - xlim[0]
        self.ylen = ylim[1] - ylim[0]
        ## for timestep calculations
        self.max_dx_one_step = self.xlen * self.CFL
        self.max_dy_one_step = self.ylen * self.CFL
        ## other parameters
        self.t = 0.0
        self.default_dt = np.min([self.xlen, self.ylen])*self.CFL*0.125
        self.closest_distance = 0.0
        
        # initialize particles' pos + vel + rad
        #self.x = self.xlen * np.random.random(self.num_par) + self.xlim[0]
        #self.y = self.ylen * np.random.random(self.num_par) + self.ylim[0]

        guess = int(np.ceil(np.sqrt(num_par)))
        temp_arr = np.linspace(0.5*self.xlen/guess, (guess-0.5)*self.xlen/guess, guess)
        self.x = np.repeat(temp_arr, guess) + self.xlim[0]
        self.y = np.repeat(np.array([temp_arr.T]), guess, axis=0).flatten() + self.ylim[0]
        self.x = self.x[:self.num_par]
        self.y = self.y[:self.num_par]
        
        self.vx = self.max_dx_one_step * np.random.random(self.num_par) \
                      + xlim[0] * self.CFL
        self.vy = self.max_dy_one_step * np.random.random(self.num_par) \
                      + ylim[0] * self.CFL
        self.rad = np.zeros(self.num_par) \
                       + np.min([self.xlen, self.ylen]) * 2.5e-2
        if radii is not None:
            if isinstance(radii, str) and radii == "random_size":
                self.rad = (np.random.random(self.num_par)*0.9+0.1) \
                               * np.min([self.xlen, self.ylen]) * 3e-2
            if isinstance(radii, np.ndarray):
                self.rad = radii
        self.max_rad = self.rad.max()
        self.half_median_rad = np.median(self.rad)/2
        self.min_rad = self.rad.min()
        self.m = self.rad ** 2
        self.m /= np.sum(self.m)
        self.ke = np.sum(0.5*self.m*(self.vx**2+self.vy**2))


        # even more members to simplify further calculations
        ## for collisional reflecting
        self.twice_xlim_col = np.zeros([2, self.num_par])
        self.twice_xlim_col[0] = self.twice_xlim[0] + 2 * self.rad
        self.twice_xlim_col[1] = self.twice_xlim[1] - 2 * self.rad
        self.twice_ylim_col = np.zeros([2, self.num_par])
        self.twice_ylim_col[0] = self.twice_ylim[0] + 2 * self.rad
        self.twice_ylim_col[1] = self.twice_ylim[1] - 2 * self.rad
        
        self.xlim_col = np.zeros([2, self.num_par])
        self.xlim_col[0] = self.xlim[0] + self.rad
        self.xlim_col[1] = self.xlim[1] - self.rad
        self.ylim_col = np.zeros([2, self.num_par])
        self.ylim_col[0] = self.ylim[0] + self.rad
        self.ylim_col[1] = self.ylim[1] - self.rad


    def timestep(self):
        """Compute timestep"""

        self.dt = self.default_dt
        max_abs_vx = np.abs(self.vx).max()
        max_abs_vy = np.abs(self.vy).max()
        if max_abs_vx * self.dt > self.max_dx_one_step:
            self.dt = self.max_dx_one_step / max_abs_vx
        if max_abs_vy * self.dt > self.max_dy_one_step:
            self.dt = self.max_dy_one_step / max_abs_vy


    def onestep_forward_collisionless(self):
        """Kick particles (collisionless) one step forward"""

        if self.BCs == "Reflecting":
            self.x += self.vx * self.dt
            out_boundary = self.x < self.xlim[0]
            self.vx[out_boundary] *= -1
            self.x[out_boundary] = self.twice_xlim[0] - self.x[out_boundary]
            out_boundary = self.x > self.xlim[1]
            self.vx[out_boundary] *= -1
            self.x[out_boundary] = self.twice_xlim[1] - self.x[out_boundary]

            self.y += self.vy * self.dt
            out_boundary = self.y < self.ylim[0]
            self.vy[out_boundary] *= -1
            self.y[out_boundary] = self.twice_ylim[0] - self.y[out_boundary]
            out_boundary = self.y > self.ylim[1]
            self.vy[out_boundary] *= -1
            self.y[out_boundary] = self.twice_ylim[1] - self.y[out_boundary]

        elif self.BCs == "Periodic":

            self.x += self.vx * self.dt
            out_boundary = self.x < self.xlim[0]
            self.x[out_boundary] += self.xlen
            out_boundary = self.x > self.xlim[1]
            self.x[out_boundary] -= self.xlen

            self.y += self.vy * self.dt
            out_boundary = self.y < self.ylim[0]
            self.y[out_boundary] += self.ylen
            out_boundary = self.y > self.ylim[1]
            self.y[out_boundary] -= self.ylen
        else:
            pass
            
        self.t += self.dt


    def collision(self, i, j, rad_sum):
        """Exchange momentum in collisions"""
        ri = vector_2D(self.x[i], self.y[i])
        vi = vector_2D(self.vx[i], self.vy[i])
        rj = vector_2D(self.x[j], self.y[j])
        vj = vector_2D(self.vx[j], self.vy[j])
        m_sum = self.m[i]+self.m[j]
        vij = vi - vj
        vji = -vij
        rij = ri - rj
        rji = -rij

        # See formulas in 
        # https://en.wikipedia.org/wiki/Elastic_collision#Two-dimensional
        new_vi = vi - 2*self.m[j]/m_sum * vij*rij / rij**2 * rij
        new_vj = vj - 2*self.m[i]/m_sum * vji*rji / rji**2 * rji

    
        # take a small step back to exactly touching state and evolve with new speeds
        # use an approximation here --> throw the term containing delta_t**2
        # but if v_ij.r is very large, this will be a really bad approximation
        #delta_t = (rad_sum**2 - rij**2) / 2 / np.abs(rij * vij)
        #rest_t = self.dt - delta_t
        #if rest_t < 0:
        #    print("Minus rest_t...")

        # We need a more accurate solution!
        coeff = [vij**2, -2*(rij*vij), rij**2-rad_sum**2]
        sln = np.roots(coeff)
        chosen_sln = sln[np.logical_and(sln>0, sln<self.dt)]
        # however, this breaks down when three bodies are too close, say a, b, and c
        # first, we deal with a and b, which puts b and c in collision state
        # calculations may show that 
        # b and c collide too heavily that even go backwards dt, rij.r still < rad_sum
        # reduce timestep can partially mitigate this issue, but not solve it!
        if len(chosen_sln) != 1:
            #ipdb.set_trace()
            #print("Inaccurate movement: sln = ", sln, ""\
            #      + "; ri = ("+"{:.4E}".format(ri.x1)+", "+"{:.4E}".format(ri.x2) \
            #      + "); rj = ("+"{:.4E}".format(rj.x1)+","+"{:.4E}".format(rj.x2)+")" \
            #      + "; dt = "+"{:.4E}".format(self.dt))
            
            # this is dirty workaround!!!
            # if we could find physical solution, then just artificially separate them
            artificial_separation = 1.01 * (rad_sum - rij.r)
            as_i = rij/rij.r * artificial_separation
            new_ri = ri + as_i * self.m[j] / m_sum
            new_rj = rj + (-as_i) * self.m[i] / m_sum
        else:
            # if we have physical solution
            delta_t = chosen_sln[0]
            rest_t = self.dt - delta_t
            new_ri = ri - vi * delta_t + new_vi * rest_t
            new_rj = rj - vj * delta_t + new_vj * rest_t

        # assign new dynamical properties
        self.x[i] = new_ri.x1
        self.y[i] = new_ri.x2
        self.x[j] = new_rj.x1
        self.y[j] = new_rj.x2
        self.vx[i] = new_vi.x1
        self.vy[i] = new_vi.x2
        self.vx[j] = new_vj.x1
        self.vy[j] = new_vj.x2


    def resolve_collisions(self):
        """Find collision events and resolve them"""

        collision_happened = False
        # sort particles in x direction and then check close targets
        sorted_x_ind = self.x.argsort()
        for i, item in enumerate(sorted_x_ind[:-1]):
            j = 1
            jtem = sorted_x_ind[i+j]
            x_diff = self.x[jtem] - self.x[item]
            rad_sum = self.rad[jtem] + self.rad[item]
            max_rad_sum = self.max_rad + self.rad[item]
            while x_diff < max_rad_sum:
                y_diff = np.abs(self.y[jtem]-self.y[item])
                if y_diff < max_rad_sum and x_diff**2 + y_diff**2 < rad_sum**2:
                    #ipdb.set_trace()
                    self.collision(item, jtem, rad_sum)
                    collision_happened = True
                if i+j+1 == self.num_par:
                    break
                j += 1
                jtem = sorted_x_ind[i+j]
                x_diff = self.x[jtem] - self.x[item]
                rad_sum = self.rad[jtem] + self.rad[item]
        return collision_happened

    def onestep_forward_collisional(self):
        """Kick particles (collisional) one step forward"""

        if self.BCs == "Reflecting":
            self.x += self.vx * self.dt
            out_boundary = self.x < self.xlim_col[0, :]
            self.vx[out_boundary] *= -1
            self.x[out_boundary] = self.twice_xlim_col[0, :][out_boundary] \
                                       - self.x[out_boundary]
            out_boundary = self.x > self.xlim_col[1, :]
            self.vx[out_boundary] *= -1
            self.x[out_boundary] = self.twice_xlim_col[1, :][out_boundary] \
                                       - self.x[out_boundary]

            self.y += self.vy * self.dt
            out_boundary = self.y < self.ylim_col[0, :]
            self.vy[out_boundary] *= -1
            self.y[out_boundary] = self.twice_ylim_col[0, :][out_boundary] \
                                       - self.y[out_boundary]
            out_boundary = self.y > self.ylim_col[1, :]
            self.vy[out_boundary] *= -1
            self.y[out_boundary] = self.twice_ylim_col[1, :][out_boundary] \
                                       - self.y[out_boundary]

            # we may have collision events after first trial
            # check until there is no collisions
            while True:
                if not self.resolve_collisions():
                    break

        if self.BCs == "Periodic":
            print("Collisional particles haven't been implemented yet.")
            exit()

        self.t += self.dt


def update_frame(fig, ax, ps, circles, ttl):
    """Update one frame of the animation depending on the backend"""

    if current_backend == "Qt5Agg" or current_backend == "Qt4Agg":
        ax.draw_artist(ax.patch)  # draw background before everything!
        for i in range(ps.num_par):
            circles[i].center = (ps.x[i], ps.y[i])
            ax.draw_artist(circles[i])
        ttl.set_text("t="+"{:.2f}".format(ps.t))
        ax.draw_artist(ttl)
        fig.canvas.update() # update canvas
        # if we want grid(), add an explicit draw() call, but it's slow
        #fig.canvas.draw()
        fig.canvas.flush_events()
    elif current_backend == "TkAgg":
        for i in range(ps.num_par):
            circles[i].center = (ps.x[i], ps.y[i])
        ttl.set_text("t="+"{:.2f}".format(ps.t))
        fig.canvas.draw()
    elif plt.get_backend() == "MacOSX":
        for i in range(ps.num_par):
            circles[i].center = (ps.x[i], ps.y[i])
        ttl.set_text("t="+"{:.2f}".format(ps.t))
        #plt.draw()
        #plt.pause(0.001)
        # The workaround of plt.draw() + plt.pause() causes annoying flicker.
        # The new workaround below gives smooth animation.
        # (from http://stackoverflow.com/a/26161347/4009531)
        fig.canvas.draw()
        fig.canvas.start_event_loop(0.0001)
    else:
        print("We only tested Qt5Agg, MacOSX and TkAgg backends.")
        print("You can add your own code to try other backends.")


def FPS(status):
    """Compute the FPS of our animation"""

    global start_time
    global frame
    if status == "start":
        start_time = time.time()
        frame = 0
    elif status == "counting":
        frame += 1
    elif status == "end":
        print("FPS: ", frame / (time.time() - start_time))


def key_press(event):
    """Handle key press events"""

    if event.key == 'q':
        fig.canvas.mpl_disconnect(oc_connection)
        plt.close("all")
        FPS("end")
        exit()


def on_close(event):
    """End the calculation if figure window is closed"""

    #event.canvas.figure.axes[0].has_been_closed = True
    plt.close("all")
    FPS("end")
    exit()

if __name__ == "__main__":

    # setup parameters
    xlim = [-2, 2]
    ylim = [-2, 2]
    num_particles = 64

    # initialize the figure frame
    fig, ax = plt.subplots(figsize=(6, 6), dpi=100)
    plt.xlim(xlim)
    plt.ylim(ylim)

    # display the frame
    if current_backend == "MacOSX":
        plt.ion()
        plt.show()
    else:
        plt.show(block=False)

    # construct the particle set and plot the first frame
    ps = ParticleSet(num_par=num_particles, xlim=xlim, ylim=ylim, radii="random_size")
    circles = []
    for i in range(ps.num_par):
        circles.append(ax.add_patch(plt.Circle((ps.x[i], ps.y[i]), ps.rad[i])))
    ttl = ax.text(0.9, 0.01, "t="+"{:.2f}".format(ps.t), horizontalalignment='center', \
                  verticalalignment='center', transform=ax.transAxes)
    fig.canvas.draw()
    if current_backend == "MacOSX":
        fig.canvas.start_event_loop(0.001)

    # connect interactive operations
    kp_connection = fig.canvas.mpl_connect('key_press_event', key_press)
    oc_connection = fig.canvas.mpl_connect('close_event', on_close)

    # begin animation and compute the FPS
    #ipdb.set_trace()
    FPS("start")
    while True:
        ps.timestep()
        ps.onestep_forward_collisional()
        update_frame(fig, ax, ps, circles, ttl)
        FPS("counting")
